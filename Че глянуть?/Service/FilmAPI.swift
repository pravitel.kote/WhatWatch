//
//  FilmAPI.swift
//  Че глянуть?
//
//  Created by Александра on 04.06.2021.
//

import Foundation

import Combine

class ApiService {
    
    private var dataTask: URLSessionDataTask?
    private let apiKey = "5a4377bdec0c86eb5bb717bcaa8beb7e"
    private let baseURL = "https://api.themoviedb.org/3"
    private let urlSession = URLSession.shared
    
    func getPopularMoviesData(completion: @escaping (Result<Film, Error>) -> Void) {
        
        let popularMoviesURL = "\(baseURL)/movie/popular?api_key=\(apiKey)&language=ru-RUS&page=\(Int.random(in: 1..<30))"
        
        guard let url = URL(string: popularMoviesURL) else {return}
        
        // Create URL Session - work on the background
        dataTask = urlSession.dataTask(with: url) { (data, response, error) in
            
            // Handle Error
            if let error = error {
                completion(.failure(error))
                print("DataTask error: \(error.localizedDescription)")
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                // Handle Empty Response
                print("Empty Response")
                return
            }
            print("Response status code: \(response.statusCode)")
            
            guard let data = data else {
                // Handle Empty Data
                print("Empty Data")
                return
            }
            
            do {
                // Parse the data
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(FilmData.self, from: data)
                

                let popularMovies = jsonData.movies
                    let popularMovie = popularMovies[Int.random(in: 0..<popularMovies.count)]
                print(popularMovie)
                
                // Back to the main thread
                DispatchQueue.main.async {
                    completion(.success(popularMovie))
                }
            } catch let error {
                completion(.failure(error))
            }
            
        }
        dataTask?.resume()
    }
}

