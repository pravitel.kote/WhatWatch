//
//  FilmTableViewCell.swift
//  Че глянуть?
//
//  Created by Александра on 04.06.2021.
//

import UIKit

class FilmTableViewCell: UITableViewCell {

    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var rateLbl: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var yearLbl: UILabel!
    @IBOutlet weak var genresLbl: UILabel!
    @IBOutlet weak var overview: UILabel!
    
    private var urlString: String = ""
    
    // Setup movies values
    func setCellWithValuesOf(_ movie: Film) {
        updateUI(title: movie.title,
                 releaseDate: movie.year,
                 rating: movie.rate,
                 overview: movie.overview,
                 poster: movie.posterImage,
                 genres: movie.genres ?? [0])
    }
    
    // Update the UI Views
    private func updateUI(title: String?,
                          releaseDate: String?,
                          rating: Double?,
                          overview: String?,
                          poster: String?,
                          genres: [Int]) {
        
        self.name.text = title
        self.overview.text = overview
        self.yearLbl.text = convertDateFormater(releaseDate)
        if let rate = rating {
        self.rateLbl.text = String(rate)
        }
        self.genresLbl.text = getMovieGenres(genres: genres)
        guard let posterString = poster else {return}
        urlString = "https://image.tmdb.org/t/p/w300" + posterString
        
        guard let posterImageURL = URL(string: urlString) else {
            self.poster.image = UIImage(named: "noImage")
            return
        }
        
        // Before we download the image we clear out the old one
        self.poster.image = nil
        
        getImageDataFrom(url: posterImageURL)
        
    }
    
    // MARK: - Get image data
    private func getImageDataFrom(url: URL) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            // Handle Error
            if let error = error {
                print("DataTask error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data else {
                // Handle Empty Data
                print("Empty Data")
                return
            }
            
            DispatchQueue.main.async {
                if let image = UIImage(data: data) {
                    self.poster.image = image
                }
            }
        }.resume()
    }
    
    private func getMovieGenres(genres: [Int]) -> String {
        var genresNames = ""
        for id in genres {
            switch  id {
            case 0:
                genresNames = "Нет информации"
            case 28:
                genresNames += "боевик, "
            case 12:
                genresNames += "приключение, "
            case 16:
                genresNames += "анимация, "
            case 35:
                genresNames += "комедия, "
            case 80:
                genresNames += "детектив, "
            case 99:
                genresNames += "документальный, "
            case 18:
                genresNames += "драма, "
            case 10751:
                genresNames += "семейный, "
            case 14:
                genresNames += "фентези, "
            case 36:
                genresNames += "исторический, "
            case 27:
                genresNames += "хоррор, "
            case 10402:
                genresNames += "музыкальный, "
            case 9648:
                genresNames += "мистика, "
            case 10749:
                genresNames += "романтика, "
            case 878:
                genresNames += "научная фантастика, "
            case 10770:
                genresNames += "телешоу, "
            case 53:
                genresNames += "триллер, "
            case 10752:
                genresNames += "военный, "
            case 37:
                genresNames += "вестерн, "
            default:
                genresNames += String()
            }
        }
        genresNames = String(genresNames.dropLast(2))
        return genresNames
    }
    
    // MARK: - Convert date format
    func convertDateFormater(_ date: String?) -> String {
        var fixDate = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let originalDate = date {
            if let newDate = dateFormatter.date(from: originalDate) {
                dateFormatter.dateFormat = "dd.MM.yyyy"
                fixDate = dateFormatter.string(from: newDate)
            }
        }
        return fixDate
    }

}
