//
//  FilmTableViewController.swift
//  Че глянуть?
//
//  Created by Александра on 04.06.2021.
//

import UIKit

class FilmTableViewController: UITableViewController {
   
    private var viewModel = FilmViewModel()
    @IBOutlet weak var filmTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPopularMoviesData()
        
    }
    
    
    @IBAction func newFilm(_ sender: Any) {
        loadPopularMoviesData()
    }
    
    @IBAction func goToImdb(_ sender: UIBarButtonItem) {
        UIApplication.shared.open(URL(string: "https://www.imdb.com/")!)
    }
    private func loadPopularMoviesData() {
        viewModel.fetchPopularMoviesData { [weak self] in
            self?.tableView.dataSource = self
            self?.tableView.reloadData()
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = filmTableView.dequeueReusableCell(withIdentifier: "FilmTableViewCell", for: indexPath) as? FilmTableViewCell else {
            fatalError()
        }
        if let movie = viewModel.popularMovie {
        cell.setCellWithValuesOf(movie)
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.height-70
    }

}
